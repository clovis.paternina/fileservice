package com.service.file.Application.services.impl;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.service.file.Application.services.RabbitMQ;
import com.service.file.Domain.model.DTO.FileDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@RabbitListener(queues = "rabbitmq.queue", id = "listener")
public class RabbitMQImpl implements RabbitMQ {

    private static Logger logger = LogManager.getLogger(RabbitMQ.class.toString());

    @Autowired
    private FileServiceImpl fileService;


    @Override
    @RabbitHandler
    public void receiver(String fileDto) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        FileDTO fileDTOImage =  objectMapper.readValue(fileDto, FileDTO.class);
        fileService.addFile(fileDTOImage);
        logger.info("se Guardo Satisfactoriamente la imagen con ID " + fileDTOImage.getId());
    }
}
