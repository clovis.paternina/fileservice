package com.service.file.Application.services.impl;

import com.service.file.Application.services.FileService;
import com.service.file.Domain.model.DTO.FileDTO;
import com.service.file.Domain.model.File;
import com.service.file.Infrastructure.rest.mapper.FileMapper.FileMapper;
import com.service.file.Infrastructure.adapter.FilesRepository;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class FileServiceImpl implements FileService {

    private FileMapper fileMapper = Mappers.getMapper(FileMapper.class);

    @Autowired
    FilesRepository filesRepository;

    public String addFile(MultipartFile file) throws IOException {

        File photo = new File();
        photo.setTitle(file.getOriginalFilename());
        photo.setImage( file.getBytes());
        photo = filesRepository.insert(photo);
        return photo.getId();

    }

    @Override
    public String addFile(FileDTO fileDTO) throws IOException {
        File photo = filesRepository.insert(fileMapper.fileDTOToFile(fileDTO));
        return photo.getId();
    }

    public FileDTO downloadFile(String id) {
       File file = filesRepository.findById(id).get();

       return fileMapper.fileToFileDTO(file);
    }

}
