package com.service.file.Application.services;

import com.service.file.Domain.model.DTO.FileDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileService {

    String addFile(MultipartFile file) throws IOException;


    String addFile(FileDTO fileDTO) throws IOException;
    FileDTO downloadFile(String id) throws IOException;

}
