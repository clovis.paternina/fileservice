package com.service.file.Application.services;

import java.io.IOException;

public interface RabbitMQ {
    void receiver (String fileDto) throws IOException;
}
