package com.service.file.Infrastructure.adapter;

import com.service.file.Domain.model.File;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilesRepository extends MongoRepository<File, String> {
}
