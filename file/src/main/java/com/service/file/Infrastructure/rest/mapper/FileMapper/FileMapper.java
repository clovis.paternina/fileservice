package com.service.file.Infrastructure.rest.mapper.FileMapper;

import com.service.file.Domain.model.File;
import com.service.file.Domain.model.DTO.FileDTO;
import org.mapstruct.Mapper;

@Mapper
public interface FileMapper {
    FileDTO fileToFileDTO(File file);
    File fileDTOToFile(FileDTO fileDTO);
}
