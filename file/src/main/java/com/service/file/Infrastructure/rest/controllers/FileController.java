package com.service.file.Infrastructure.rest.controllers;

import com.service.file.Domain.model.DTO.FileDTO;
import com.service.file.Application.services.impl.FileServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class FileController {

    @Autowired
    private FileServiceImpl fileService;

    @PostMapping("/upload")
    public ResponseEntity<?> upload(@RequestParam("image") MultipartFile image) throws IOException {

        return new ResponseEntity<>(fileService.addFile(image), HttpStatus.OK);
    }

    @GetMapping("/download/{id}")
    public ResponseEntity<FileDTO> download(@PathVariable String id) {
        return new ResponseEntity<>(fileService.downloadFile(id), HttpStatus.OK);

    }
}
