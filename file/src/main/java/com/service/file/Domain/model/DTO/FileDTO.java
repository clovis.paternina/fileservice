package com.service.file.Domain.model.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FileDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String title;
    @JsonProperty("image")
    private byte[] image;

}
